import React, { Component } from "react";
import { Link } from "react-router-dom";
import { addUpdateChart } from "../Api/chart.api";

export default class Sidebar extends Component {
    createChart = async (type = "FC") => {
        let json = {
            "chart_id": null,
            "product": type,
            "is_template": false,
            "is_starred": false,
            "pages_id": null,
            "chart": {},
            "page_name": "Page 1",
            "chart_name": "Blank diagram"
        }
        let response = await addUpdateChart(json);

        if (response.success) {
            type === "FC" 
                ? this.props.history.push(`/canvas/${response.data.data.chart_id}`)
                : this.props.history.push(`/whiteboard/${response.data.data.chart_id}`);
        } else {
            // alert(response.data.msg);
        }
    }
    // createWhiteBoard = () => {
    //     let {history} = this.props;

    //     history.push(`/whiteboard/1`);
    // }
    render() {
        return (
            <aside className="dashboard-sidebar">
                <div className="d-grid">
                    <div className="btn-group w-100">
                        <button type="button" className="btn btn-dark d-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            + Create
                        </button>
                        <div className="dropdown-menu w-100">
                            {/* <Link to="/canvas" class="dropdown-item">NEW FLOWCHART</Link> */}
                            <button onClick={() => this.createChart('FC')} class="dropdown-item">NEW FLOWCHART</button>
                            <button onClick={() => this.createChart('WB')} class="dropdown-item">NEW WHITEBOARD</button>
                            {/* <a class="dropdown-item" href="#">NEW WHITEBOARD</a> */}
                        </div>
                    </div>
                </div>
                <ul>
                    <li>
                        <Link to="/">Dashboard</Link>
                    </li>
                    <li>
                        <Link to="/">For You</Link>
                    </li>
                    <li>
                        <Link to="/">Templates</Link>
                    </li>
                </ul>
                <button className="btn btn-success btn-block">Upgrade</button>
            </aside>
        );
    }
}
