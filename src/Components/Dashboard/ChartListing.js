import React, { Component } from "react"
import { foramatedDate } from "../../Configs/date.config";

export default class ChartListing extends Component {
    render() {
        return (
            <>
                {
                    this.props.charts.map(function (row, i) {
                        return (
                            <div key={i}>
                                <h5>{row.title}</h5>
                                {
                                    row.data && row.data.length
                                        ? <div className="row dashboard-blocks">
                                            {
                                                row.data.map((ch, j) => {
                                                    return <div className="col-sm-3 mb-3" key={j}>
                                                        <div className="card overflow-hidden">
                                                            <div className="card-body">
                                                                <div className="py-3">
                                                                    <span className="bg-secondary rounded px-2 text-white">
                                                                        { ch.product === "FC" ? "Flowchart" : "Whiteboard" }
                                                                    </span>
                                                                    <h4>{ch.chart_name && ch.chart_name.trim() !== '' ? ch.chart_name : "Blank diagram"}</h4>
                                                                    <div>
                                                                        <i className="icon-user2"></i> {ch.name}
                                                                    </div>
                                                                    <div>
                                                                        <i className="icon-clock1"></i> {foramatedDate(ch.date_modified)}
                                                                    </div>
                                                                    {
                                                                        ch.access_type ?
                                                                            <div>
                                                                                <i className="icon-share2"></i> {ch.access_type}
                                                                            </div>
                                                                        : <></>
                                                                    }
                                                                </div>
                                                            </div>
                                                            <div className="p-3 bg-dark text-white card-heading">
                                                                { !ch.access_type || ch.access_type === "EDIT" ? "Edit" : "Open"} Now
                                                            </div>
                                                            <a href={ch.product === "FC" ? `canvas/${ch.chart_id}` : `whiteboard/${ch.chart_id}`}></a>
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                        : <div>No records created yet in <strong>{row.title}</strong>.</div>
                                }

                            </div>
                        )
                    })
                }
            </>
        );
    }
}
