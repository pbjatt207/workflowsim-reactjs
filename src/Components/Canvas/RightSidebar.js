import React, { Component } from "react";
import { addUpdateFlowchart, getFlowchart } from "../../Api/flowchart.api";

export default class CanvasRightSidebar extends Component {
    state = {
        flowchartPopupOpen: false,
        formData: {
            name: '',
            description: ''
        },
        workflows: []
    }

    activeWorkflow = (e, id) => {
        e.preventDefault();
        this.props.activeWorkflow(id);
    }

    playWorkflow = (e, id) => {
        e.preventDefault();
        this.props.playWorkflow(id);
    }

    playPauseWorkflow = e => {
        this.props.playPauseWorkflow(e);
    }

    deleteWorkflow = async row => {
        if(window.confirm("Are you sure remove workflow?"))
        {
            var data = {
                "workflow_id": row.workflow_id,
                "pages_id": row.pages_id,
                "workflow_name": row.workflow_name,
                "workflow_description": row.workflow_description,
                "workflow": row.workflow,
                "is_active": false
            }
            let response = await addUpdateFlowchart(data);
    
            this.setState({
                workflows: response.data.data,
                formData: {
                    name: '',
                    description: ''
                }
            });
        }
    }

    handleInput = (e, feild) => {
        let formData = this.state.formData;
        formData[feild] = e.target.value;
        this.setState({formData});
    }

    toggleFlowChart = e => {
        e.preventDefault();
        document.getElementById('flowchart-popup').classList.toggle("shown");
    }

    handleSpeechOptions = (value, field) => {
        let { speechOptions, speech } = this.props.state;
        speechOptions[field] = value;
        
        switch (field) {
            case 'volume':
                speech.setVolume(value);
                break;

            case 'voice':
                speech.setVoice(value);
                break;
        
            default:
                break;
        }

        this.props.setStateValue({ speechOptions });
    }

    creatFlowchart = async e => {
        e.preventDefault();
        let state = this.props.state;
        let { name, description } = this.state.formData
        var data = {
            "workflow_id": null,
            "pages_id": state.chart.pages_id,
            "workflow_name": name,
            "workflow_description": description,
            "workflow": state.workflowShapes,
            "is_active": true
        }
        let response = await addUpdateFlowchart(data);

        this.setState({
            workflows: response.data.data,
            formData: {
                name: '',
                description: ''
            }
        });
        
        this.props.setStateValue({
            workflowShapes: [],
            workflows: response.data.data
        });

        document.getElementById('flowchart-popup').classList.remove("shown");
    }

    goToIndex = type => {
        let { workflowIndex, workflowPlayList } = this.props.state;

        workflowIndex = parseInt(workflowIndex);

        // console.log('current workflow index: ', workflowIndex);
        if (type === 'prev' && workflowIndex) {
            workflowIndex--;
        }
        if (type === 'next' && workflowIndex < workflowPlayList.length - 1) {
            workflowIndex++;
        }

        // console.log('new workflow index: ', workflowIndex);


        this.props.setStateValue({
            workflowIndex
        });

        setTimeout(() => {
            this.props.playPauseWorkflow(type, false);
        }, 10);

    }

    render() {
        let state = this.props.state;
        return (
            <>
                <div className="chart-right-sidebar">
                    <div>
                        <button className="btn btn-block" onClick={() => {
                            this.props.setStateValue({
                                flowchartBoardOpen: !state.flowchartBoardOpen
                            });
                        }}><i className="icon-play2"></i></button>
                        <button className="btn btn-block"><i className="icon-stack"></i></button>
                        <button className="btn btn-block"><i className="icon-display"></i></button>
                        <button className="btn btn-block"><i className="icon-comment1"></i></button>

                        <div className={!state.flowchartBoardOpen ? "flowchart-board" : "flowchart-board active"}>
                            {
                                state.chart.access_type === 'EDIT' ?
                                    <button className="btn btn-block btn-primary btn-sm" style={{cursor: "pointer"}} onClick={e => this.props.activeWorkflow(null)}>Create new workflow</button>
                                : <></>
                            }

                            <ul className="workflow-lists">
                                {
                                    state.workflows && state.workflows.length && state.workflows.map((row, i) => {
                                        return (
                                            <li>
                                                <div className="d-flex">
                                                    <a href="#" key={i} className="flex-fill workflow-name" onClick={e => this.activeWorkflow(e, row.workflow_id)}>{row.workflow_name}</a>
                                                    <a className="btn btn-sm btn-primary" onClick={e => this.playWorkflow(e, row)}>
                                                        <i className="icon-play2"></i>
                                                    </a>
                                                    {
                                                        state.chart.access_type === 'EDIT' ?
                                                        <>
                                                            <a className="btn btn-sm btn-info">
                                                                <i className="icon-edit1"></i>
                                                            </a>
                                                            <a className="btn btn-sm btn-danger" onClick={e => this.deleteWorkflow(row)}>
                                                                <i className="icon-trash1"></i>
                                                            </a>
                                                        </>
                                                        : <></>
                                                    }
                                                </div>
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                    <button type="button" className="btn btn-block" onClick={() => this.props.toggleFullScreen()}><i className={!state.isFullScreen ? "icon-fullscreen" : "icon-fullscreen_exit"}></i></button>
                </div>
                <div id="flowchart-popup">
                    <div className="popup-bg" onClick={e => this.toggleFlowChart(e)}></div>
                    <div className="popup-part">
                        <div className="popup-body">
                            <a href="#close" className="close" onClick={e => this.toggleFlowChart(e)}>&times;</a>
                            <form method="POST" onSubmit={e => this.creatFlowchart(e)}>
                                <div className="form-group">
                                    <label>Workflow Name</label>
                                    <input name="name" className="form-control" placeholder="Workflow Name" onInput={e => this.handleInput(e, 'name')} />
                                </div>
                                <div className="form-group">
                                    <label>Workflow Description</label>
                                    <textarea rows="5" name="description" className="form-control" placeholder="Workflow Description" onInput={e => this.handleInput(e, 'description')}></textarea>
                                </div>
                                <div>
                                    <button type="submit" className="btn btn-primary btn-block">Save Workflow</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {
                    state.workflowsStart && !state.workflowsPlayMode
                    ? <div className="flowchart-save-btn">
                        <button className="btn btn-primary" onClick={(e) => this.toggleFlowChart(e)}>Save &amp; Update</button>
                    </div>
                    : <></>
                }
                {
                    state.workflowsStart && state.workflowsPlayMode
                    ? <div className="flowchart-save-btn workflow-btn-group">
                        <button type="button" title={!state.workflowsPaused ? "Play" : "Pause"} onClick={(e) => this.playPauseWorkflow(e)}>
                            {
                                <i className={!state.workflowsPaused ? "icon-play3" : "icon-pause"}></i>
                            }
                        </button>
                        <button 
                            type="button" 
                            title="Previous" 
                            disabled={!state.workflowIndex}
                            onClick={() => this.goToIndex('prev')}
                        >
                            <i className="icon-skip_previous"></i>
                        </button>
                        <button 
                            type="button" 
                            title="Next" 
                            disabled={state.workflowIndex === state.workflowPlayList.length - 1}
                            onClick={() => this.goToIndex('next')}
                        >
                            <i className="icon-skip_next"></i>
                        </button>
                        <input type="range" min="0" max="1" value={state.speechOptions.volume} step="0.1" onChange={e => this.handleSpeechOptions(e.target.value, 'volume')} />
                        <select id="narator-list" value={state.speechOptions.voice} onChange={e => this.handleSpeechOptions(e.target.value, 'voice')}>
                            <option value="Google UK English Female">Female</option>
                            <option value="Google UK English Male">Male</option>
                        </select>
                    </div>
                    : <></>
                }
            </>
        );
    }
}
