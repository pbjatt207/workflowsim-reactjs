const { default: apiExecute } = require(".")

export const LoginApi = async (data) => {
    return apiExecute('login', 'POST', { data: data })
}

export const SocialLoginApi = async (data) => {
    return apiExecute('social-login', 'POST', { data: data })
}

export const RegisterApi = async (data) => {
    return apiExecute('register', 'POST', { data: data })
}

export const ProfileApi = async () => {
    return apiExecute('profile', 'GET', { auth: true })
}

// module.exports = {
//     LoginApi,
//     SocialLoginApi,
//     RegisterApi,
//     ProfileApi
// }
