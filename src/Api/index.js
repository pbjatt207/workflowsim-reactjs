import axios from "axios";

const apiBaseUrl = '/api/';

const apiExecute = async (url, method = 'GET', params = null) => {
    let headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }

    if (params && params.auth) {
        let token = localStorage.getItem('token');
        if (!token || token === 'null') {
            token = sessionStorage.getItem('token');
        }
        headers["x-access-token"] = token;
    }

    
    if(params && params.files) {
        headers["Content-Type"] = "multipart/form-data";
    }

    return axios({
        url: `${apiBaseUrl}${url}`,
        method,
        data: params && params.data ? params.data : null,
        headers
    })
        .then(res => {
            return { success: 1, data: res.data };
        })
        .catch(err => {
            return { success: 0, data: err.response.data };
        })
}

export default apiExecute;
