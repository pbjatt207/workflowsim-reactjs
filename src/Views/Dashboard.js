import React, { Component } from "react"
import { getCharts } from "../Api/chart.api"
import Header from "../Components/Header"
import Sidebar from "../Components/Sidebar"
import ChartListing from "../Components/Dashboard/ChartListing";
import Loading from "../Components/Loading";

export default class Dashboard extends Component {
    state = {
        charts: [],
        loading: true
    }
    async componentDidMount() {
        let res = await getCharts();
        this.setState({ loading: false });

        if (res.success) {
            this.setState({
                charts: res.data
            })
        }
    }
    render() {
        return (
            <div>
                <Header {...this.props} />
                <div className="d-flex">
                    <Sidebar {...this.props} />
                    <div className="dashboard-content bg-light">
                        {
                            !this.state.loading
                                ?
                                this.state.charts.length
                                    ? <ChartListing charts={this.state.charts} />
                                    : <div>No charts owned by you yet.</div>
                                : <Loading />
                        }
                    </div>
                </div>
            </div>
        )
    }
}
