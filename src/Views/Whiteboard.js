import { Component } from 'react';
import { addUpdateChart, getChartInfo } from '../Api/chart.api';
import WhiteboardHeader from '../Components/Whiteboard/Header';
import WhiteboardArea from '../Components/Whiteboard/WhiteboardArea';

class Whiteboard extends Component {
    state = {
        mode: 'pencil',
        penColor: '#ff0000',
        chart: {}
    }

    updateState = json => {
        let self = this;
        this.setState(json);

        setTimeout(() => {
            self.saveCanvas();
        }, 100);
    }

    saveCanvas = () => {
        let { chart } = this.state;

        addUpdateChart(chart);
    }

    componentDidMount = async () => {
        let { whiteboardId } = this.props.match.params;
        let chart = await getChartInfo(whiteboardId);
        
        this.setState({ chart: chart.data });
    }

    render() {
        return (
            <div className='canvas-screen whiteboard'>
            {
                this.state.chart.chart
                ? <>
                    <WhiteboardHeader {...this.props} state={this.state} updateState={json => this.updateState(json)}  />

                    <div className="d-flex chart-main-content">
                        <WhiteboardArea {...this.props} state={this.state} updateState={json => this.updateState(json)} />
                    </div>
                </>
                : <></>
            }
            </div>
        );
    }
}

export default Whiteboard;
