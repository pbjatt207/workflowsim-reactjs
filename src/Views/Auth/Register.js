import { Component } from "react";
import { Link } from "react-router-dom";
import { RegisterApi, LoginApi } from "../../Api/auth.api";
import SocialLogin from "./SocialLogin";
import queryString from 'query-string';
class Register extends Component {
    state = {
        alert: {
            type: null,
            msg: null
        }
    }
    handleSubmit = async (e) => {
        e.preventDefault();
        let search = this.props.location.search;

        const { history } = this.props;

        this.setState({
            alert: {
                type: 'info',
                msg: 'Progressing! Please wait...'
            }
        });

        var formdata = new FormData(e.target);
        let query = queryString.parse(search);
        if (query.key) formdata.append('share_token', query.key);

        var data = await RegisterApi(Object.fromEntries(formdata));

        this.setState({
            alert: {
                type: data.success ? 'success' : 'danger',
                msg: data.data.msg
            }
        });

        if (data.success) {
            var formdetail = Object.fromEntries(formdata);
            var detail = {
                login: formdetail.email,
                password: formdetail.password,
            }
            var login_data = await LoginApi(detail);

            if (login_data.success) {
                this.setState({
                    alert: {
                        type: 'success',
                        msg: login_data.data.msg
                    }
                })
                this.AfterloginRedirect(login_data);
            }
        }
        // } history.push('/login');
    }
    AfterloginRedirect = (data, remember_me = true) => {
        const { history } = this.props;

        if (data.data.accessToken && typeof data.data.accessToken !== 'undefined') {
            localStorage.removeItem('token');
            sessionStorage.removeItem('token');

            if (remember_me) {
                localStorage.setItem('token', data.data.accessToken);
            } else {
                sessionStorage.setItem('token', data.data.accessToken);
            }

            localStorage.setItem('user', JSON.stringify(data.data.data));

            this.setState({
                alert: {
                    type: 'success',
                    msg: data.data.msg
                }
            });

            // this.props.onLogin(data.data.accessToken);

            history.push('/');
        } else {
            this.setState({
                alert: {
                    type: 'danger',
                    msg: "something went wrong, try again."
                }
            })
        }
    }

    loginRedirect = (data) => {
        const { history } = this.props;
        localStorage.setItem('token', data.accessToken);

        this.setState({
            alert: {
                type: 'success',
                msg: data.msg
            }
        })
        history.push('/');
    }

    render() {
        let alertBox = null, disabled = false;
        let search = this.props.location.search;
        let query = queryString.parse(search);

        if (this.state.alert.type) {
            alertBox = <div className={`alert alert-${this.state.alert.type}`}>{this.state.alert.msg}</div>
        }
        if (this.state.alert.type === 'info') {
            disabled = 'disabled';
        }
        return (
            <div className='vh-100 bg-primary d-flex align-items-center justify-content-center'>
                <div className='card' style={{ width: 400 }}>
                    <div className='card-body'>
                        <form method='post' autoComplete='off' onSubmit={this.handleSubmit}>
                            {alertBox}
                            <div className='form-group'>
                                <input type='text' name='name' className='form-control' placeholder='Enter Name' required />
                            </div>
                            <div className='form-group'>
                                <input type='email' name='email' className='form-control' placeholder='Enter Email Address' required />
                            </div>
                            <div className='form-group'>
                                <input type='password' name='password' className='form-control' placeholder='Enter Password' autoComplete='new-password' required />
                            </div>
                            <div className='form-group'>
                                <input type='password' className='form-control' placeholder='Confirm Password' autoComplete='new-password' required />
                            </div>
                            <button type='submit' className='btn btn-primary btn-block' disabled={disabled}>Register</button>
                            <div className='text-center mt-1'>
                                Already have an account? <Link to='/login'>Login here</Link>.
                            </div>
                            <hr />
                            <div className='text-center'>
                                <h5>Register with</h5>
                                <SocialLogin onLoginSuccess={(data) => this.loginRedirect(data)} shareToken={query.key}></SocialLogin>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        )
    }
}

export default Register;
